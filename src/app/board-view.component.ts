import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-board-view',
  template: `
    <div class="page-content">

      <h1><a routerLink="/">boards</a> / <strong>[Board Name]</strong></h1>

      <section class="board-table">
        <div class="column">
          <h2>To Do</h2>
          <div class="cell">
            <db-task></db-task>
            <db-task></db-task>
          </div>
        </div>

        <div class="column">
          <h2>In Progress</h2>
          <div class="cell">
            <db-task></db-task>
            <db-task></db-task>
          </div>
        </div>

        <div class="column">
          <h2>Done</h2>
          <div class="cell">
            <db-task></db-task>
            <db-task></db-task>
          </div>
        </div>
      </section>

      <div class="fab-button-container">
        <button
          md-button
          md-fab
          routerLink="/new-task"><md-icon>add</md-icon></button>
      </div>
    </div>
  `,
  styles: []
})
export class BoardViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
