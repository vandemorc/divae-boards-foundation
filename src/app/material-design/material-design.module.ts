import { NgModule } from '@angular/core';
import {
  MdButtonModule,
  MdIconModule,
  MdToolbarModule,
  MdCardModule,
  MdInputModule,
  MdSelectModule,
  MdTooltipModule,
  MdProgressSpinnerModule,
} from '@angular/material';

const components = [
  MdButtonModule,
  MdCardModule,
  MdInputModule,
  MdSelectModule,
  MdIconModule,
  MdToolbarModule,
  MdSelectModule,
  MdTooltipModule,
  MdProgressSpinnerModule,
];

@NgModule({
  imports: components,
  exports: components,
})
export class MaterialDesignModule { }
