import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-new-task',
  template: `
    <div class="page-content">
      <h1>Create new task on [Board Name]</h1>
      <md-card>
        <md-card-content>
          <md-input-container>
            <input mdInput placeholder="Task Name">
          </md-input-container>
        </md-card-content>
        <md-card-actions>
          <button md-button>BACK</button>
          <button md-button>ADD</button>
        </md-card-actions>
      </md-card>
    </div>
  `,
  styles: []
})
export class NewTaskComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
