import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-dashboard',
  template: `
    <div class="page-content">
      <h1>My Boards</h1>
      <!--boards-->
      <db-dashboard-board></db-dashboard-board>
      <db-dashboard-board></db-dashboard-board>
    </div>

    <!--( + ) button-->
    <div class="fab-button-container">
      <button
        md-button
        md-fab
        routerLink="/new-board"><md-icon>add</md-icon></button>
    </div>
  `,
  styles: []
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
