import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {NewBoardComponent} from './new-board.component';
import {EditBoardComponent} from './edit-board.component';
import {BoardViewComponent} from './board-view.component';
import {NewTaskComponent} from './new-task.component';
import {EditTaskComponent} from './edit-task.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DashboardComponent,
  }, {
    path: 'new-board',
    component: NewBoardComponent,
  }, {
    path: 'edit-board',
    component: EditBoardComponent,
  }, {
    path: 'board',
    component: BoardViewComponent,
  }, {
    path: 'new-task',
    component: NewTaskComponent,
  }, {
    path: 'edit-task',
    component: EditTaskComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
