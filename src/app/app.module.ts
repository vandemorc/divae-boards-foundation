import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { MaterialDesignModule } from './material-design/material-design.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard.component';
import { BoardViewComponent } from './board-view.component';
import { NewBoardComponent } from './new-board.component';
import { EditBoardComponent } from './edit-board.component';
import { NewTaskComponent } from './new-task.component';
import { EditTaskComponent } from './edit-task.component';
import { TaskComponent } from './task.component';
import { DashboardBoardComponent } from './dashboard-board.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    BoardViewComponent,
    NewBoardComponent,
    EditBoardComponent,
    NewTaskComponent,
    EditTaskComponent,
    TaskComponent,
    DashboardBoardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialDesignModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
