import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-edit-task',
  template: `
    <div class="page-content">
      <h1>Edit task of [Board Name]</h1>
      <md-card>
        <md-card-content>
          <md-input-container>
            <input mdInput placeholder="Task Name">
          </md-input-container>

          <md-select placeholder="State" name="task_state" value="todo">
            <md-option value="todo">
              Todo
            </md-option>
            <md-option value="in_progress">
              In Progress
            </md-option>
            <md-option value="done">
              Done
            </md-option>
          </md-select>
        </md-card-content>
        <md-card-actions>
          <button md-button>BACK</button>
          <button md-button>DELETE</button>
          <button md-button>UPDATE</button>
        </md-card-actions>
      </md-card>
    </div>
  `,
  styles: []
})
export class EditTaskComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
