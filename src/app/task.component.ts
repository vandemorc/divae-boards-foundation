import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-task',
  template: `
    <a routerLink="/edit-task">
      <md-card>
        <p>[Task Name]</p>
      </md-card>
    </a>
  `,
  styles: []
})
export class TaskComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
