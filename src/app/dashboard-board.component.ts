import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-dashboard-board',
  template: `
    <md-card routerLink="board" class="dashboard-board">
      <h4>[Board Name]</h4>
      <a routerLink="edit-board" class="edit">
        <md-icon>edit</md-icon>
      </a>
    </md-card>
  `,
  styles: []
})
export class DashboardBoardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
