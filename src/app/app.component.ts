import { Component } from '@angular/core';

@Component({
  selector: 'db-root',
  template: `
    <md-toolbar class="top-panel">
      <a routerLink="/">
        <img src="assets/logo-nav.svg"><span>Boards</span>
      </a>
    </md-toolbar>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'db';
}
