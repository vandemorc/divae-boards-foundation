import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-new-board',
  template: `
    <div class="page-content">
      <h1>New Board</h1>
      <md-card>
        <md-card-content>
          <md-input-container>
            <input mdInput placeholder="Board Name">
          </md-input-container>
        </md-card-content>
        <md-card-actions>
          <button md-button routerLink="/">CANCEL</button>
          <button md-button>ADD</button>
        </md-card-actions>
      </md-card>
    </div>
  `,
  styles: []
})
export class NewBoardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
