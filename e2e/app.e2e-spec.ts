import { DivaeBoardsPage } from './app.po';

describe('divae-boards App', () => {
  let page: DivaeBoardsPage;

  beforeEach(() => {
    page = new DivaeBoardsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to db!!');
  });
});
